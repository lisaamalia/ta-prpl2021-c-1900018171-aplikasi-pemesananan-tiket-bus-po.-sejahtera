<?php
session_start();

$id=$_SESSION["id"];
$username=$_SESSION["username"];
$nama=$_SESSION["nama"];
$email=$_SESSION["email"];

?>
<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
    ul{
            border-radius: 10px;
            list-style-type: none;
            margin: 0;
            padding: 1px;
            overflow: hidden;
            background-color: #324851;
        }
        li{
            float: left;
        }
        li a{
            display: block;
            color: white;
            text-align: center;
            padding: 10px 35px 10px;
            text-decoration: none;
        }
        li a:hover{
            background-color: #111111;
        }
        .edit_btn {
            text-decoration: none;
            padding: 2px 5px;
            background: #2E8B57;
            color: white;
            border-radius: 5px;
        }
        .del_btn {
            text-decoration: none;
            padding: 2px 5px;
            color: white;
            border-radius: 5px;
            background: #800000;
}
</style>
</head>
<body>
<div>
    <div>
        <ul>
            <li><a href="homeadmin.php">HOME</a></li>
            <li><a href="adminedit.php">EDIT PEMESANAN TIKET </a></li>
            <li style="float: right;"><a href="index.php">LOGOUT</a></li>
        </ul>
    </div>
    
    <div>
        <hr>
    </div>
    <p><form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
        <div style="text-align: center;">
            <h2 style="font-family: sans-serif; font-style: italic;">MENU EDIT JADWAL KEBERANGKATAN BUS PO. SEJAHTERA</h2>
            
            <label for="sel1"><h3>Pencarian Pada Kolom:</h3></label>
                <p><?php
                    $kota_keberangkatan="";
                    $kota_tujuan="";
                    $tgl="";
                    if (isset($_POST['kata_kunci'])) {
                        $kata_kunci=trim($_POST['kata_kunci']);
                        if ($_POST['kolom']=="kota_keberangkatan")
                        {
                            $kolom="kota_keberangkatan";
                        }else if ($_POST['kolom']=="kota_tujuan"){
                            $kolom="kota_tujuan";
                        }else {
                            $kolom="tgl";
                        }
                        $sql="select * from jadwal where $kolom like '%".$kata_kunci."%' order by tgl asc";
                    }
                    else {
                        $sql="select * from jadwal order by tgl asc";
                    }
                ?></p>
            <select name="kolom" required style="width: 450px; height: 25px;">
                <option value="" >Silahkan pilih kolom terlebih dahulu</option>
                <option value="nik" <?php echo $kota_keberangkatan; ?> >Kota Keberangkatan</option>
                <option value="nama" <?php echo $kota_tujuan; ?> >Kota Tujuan</option>
                <option value="tgl" <?php echo $tgl; ?> >Tanggal</option>
            </select>
        </div>
        <div style="text-align: center;">
            <p><label for="sel1"><h3>Kata Kunci:</h3></label>
            <p><?php
                $kata_kunci="";
                if (isset($_POST['kata_kunci'])) {
                    $kata_kunci=$_POST['kata_kunci'];
                }
            ?>
            <input type="text" name="kata_kunci" value="<?php echo $kata_kunci;?>"  required style="width: 445px; height: 17px;"/></p></p>
        </div>

        <div style="text-align: center; ">
            <input type="submit" value="Cari" style="background-color: #336B87; color: white; height: 25px; width: 45px; border-radius: 5px;">
        </div>
    </form></p>
    <table border="1" width="1300px" style="margin-left: auto; margin-right: auto; text-align: center;">
        <br>
        <thead>
        <tr style="background-color: #336B87; height: 5px; color: white;">
            <th>No</th>
            <th>Kode Bus</th>
            <th>Kota Keberangkatan</th>
            <th>Kota Tujuan</th>
            <th>Tanggal</th>
            <th>Jam</th>
            <th>Harga</th>
            <th>Sisa</th>
            <th>Aksi</th>
        </tr>
        </thead>
        <?php
            include "config.php";
            if (isset($_POST['kata_kunci'])) {
                $kata_kunci=trim($_POST['kata_kunci']);
                $sql="select * from jadwal where kota_keberangkatan like '%".$kata_kunci."%' or kota_tujuan like '%".$kata_kunci."%' or tgl like '%".$kata_kunci."%'order by tgl asc";
            }else {
                $sql="select * from jadwal order by tgl asc";
            }
            $hasil=mysqli_query($koneksi,$sql);
            $no=0;
            while ($data = mysqli_fetch_array($hasil)) {
            $no++;
        ?>
        <tbody>
        <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $data["kode_bus"]; ?></td>
            <td><?php echo $data["kota_keberangkatan"];   ?></td>
            <td><?php echo $data["kota_tujuan"];   ?></td>
            <td><?php echo $data["tgl"];   ?></td>
            <td><?php echo $data["jm"];   ?></td>
            <td><?php echo $data["harga"];   ?></td>
            <td><?php echo $data["sisa"];   ?></td>
            <td><?php 
            echo "<a href='form_edit.php?id=".$data['kode_bus']."' class='edit_btn'>Edit</a> | ";
            echo "<a href='delete.php?id=".$data['kode_bus']."' class='del_btn'>Hapus</a>"; ?>
            </td>
        </tr>
        </tbody>
        <?php } ?>
    </table>
</div>
<div style="padding-top: 50px;text-align: center; font-family: sans-serif;"><p><footer><b>Copyright &copy 2021 | Lisa Amalia</footer></p></div>
</body>
</html>