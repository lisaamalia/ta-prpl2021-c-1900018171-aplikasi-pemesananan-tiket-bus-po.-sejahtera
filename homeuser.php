<?php
session_start();

if (!isset($_SESSION["username"])) {
	echo "Anda harus login dulu <br><a href='proses_login.php'>Klik disini</a>";
	exit;
}

$level=$_SESSION["level"];

if ($level!="Pembeli") {
    echo "Anda tidak punya akses pada halaman pembeli";
    exit;
}

$id=$_SESSION["id"];
$username=$_SESSION["username"];
$nama=$_SESSION["nama"];
$email=$_SESSION["email"];

?>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
    ul{
            border-radius: 10px;
            list-style-type: none;
            margin: 0;
            padding: 1px;
            overflow: hidden;
            background-color: #324851;
        }
        li{
            float: left;
        }
        li a{
            display: block;
            color: white;
            text-align: center;
            padding: 10px 35px 10px;
            text-decoration: none;
        }
        li a:hover{
            background-color: #111111;
        }
</style>

</head>
<body>
<div>
        <ul>
            <li><a href="homeuser.php">HOME</a></li>
            <li><a href="pesan_tiket.php"> PEMESANAN TIKET </a></li>
            <li><a href="jadwal_keberangkatan.php">JADWAL KEBERANGKATAN</a></li>
            <li><a href="history.php">HISTORY PEMESANAN TIKET</a></li>
            <li style="float: right;"><a href="index.php">LOGOUT</a></li>
        </ul>
</div>
<div>
    <hr>
</div>
<div style="text-align: center; padding-top: 30px;">
    <h1>Selamat Datang <?php echo $nama; ?> !</h1>
    <h3>Halaman ini hanya dapat diakses oleh Anda</h3>
    <p><img src="image/profil.jpg" width="100px;" height="75px;"></p>
    <h4>Nama    : <?php echo $nama; ?></h4>
    <h4>Username : <?php echo $username; ?></h4>
    <h4>Email   : <?php echo $email; ?></h4>
</div>
<div style="padding-top: 40px;text-align: center; font-family: sans-serif;"><footer><b>Copyright &copy 2021 | Lisa Amalia</footer></div>
    

</body>
</html> 
