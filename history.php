<?php
include "config.php";
?>
 
<!DOCTYPE html>
<html>
<head>
    <title>HISTORY PEMESANAN</title>
    <style type="text/css">
    ul{
        border-radius: 10px;
        list-style-type: none;
        margin: 0;
        padding: 1px;
        overflow: hidden;
        background-color: #324851;
    }
    li{
        float: left;
    }
    li a{
        display: block;
        color: white;
        text-align: center;
        padding: 10px 35px 10px;
        text-decoration: none;
    }
    li a:hover{
        background-color: #111111;
    }
</style>
</head>
<body>
<div>
    <ul>
        <li><a href="homeuser.php">HOME</a></li>
        <li><a href="pesan_tiket.php"> PEMESANAN TIKET </a></li>
        <li><a href="jadwal_keberangkatan.php">JADWAL KEBERANGKATAN</a></li>
        <li><a href="history.php">HISTORY PEMESANAN TIKET</a></li>
        <li style="float: right;"><a href="index.php">LOGOUT</a></li>
    </ul>
</div>
<div>
    <hr>
</div>
 
<h1 style="text-align: center;">History Pemesanan</h1>
<?php
 
// menampilkan seluruh isi database
$query ="select * from pemesanan";
$hasil = mysqli_query($koneksi, $query);

while($data = mysqli_fetch_array($hasil))
{
  
  echo "<p></p>";
  echo "<div style='text-align: center;''>
  --------------------------------------------------------------------------------------------------------------------------------------------------------
  <h3>PO.SEJAHTERA</h3>
  <p>
  Jakarta-Bandung-Bogor-Tegal-Purwokerto-Kebumen-Purworejo-Yogyakarta-Solo-Semarang-Malang-Surabaya
  </p>--------------------------------------------------------------------------------------------------------------------------------------------------------</div>";
  echo "<table style='padding-left: 330px;'>";
  echo "<tr><td style='font-style: italic; font-weight: bold;'>Tanggal Pemesanan : ". date("d/m/Y")."</td>";
  echo "<tr><td>ID Pembeli : $data[id_pembeli]</tr>";
  echo "<tr><td>Nama : $data[nama]<td>";
  echo "Jumlah Tiket : $data[jml]</tr>";
  echo "<tr><td>Kota Keberangkatan : $data[kota_berangkat]</td>";
  echo "<td>No. Kursi : $data[no_kursi]</td></tr>";
  echo "<tr><td>Kota Tujuan : $data[kota_tujuan]<td/>";
  echo "Harga Tiket : $data[harga]</tr>";
  echo "<tr><td>Tanggal : $data[tgl]</td>";
  echo "<td>Diskon : $data[diskon]</td></tr>";
  echo "<tr><td>Jam : $data[jm]</td>";
  echo "<td>Total Bayar : $data[total]</td></tr>";
  echo "<tr><td>*PERHATIAN<td></tr>";
  echo "<tr><td>Penumpang harus tiba sesuai dengan waktu yang telah ditentukan.</td></tr>";
  echo "</table>"; 
  echo "<div style='text-align:center; font-style: italic; font-weight: bold; padding-bottom: 20px;'><br>Terimakasih Telah Melakukan Pemesanan</div>";
}
?>
<div style="padding-top: 40px;text-align: center; font-family: sans-serif;"><footer><b>Copyright &copy 2021 | Lisa Amalia</footer></div>
  </body>
</html>
