<!DOCTYPE html>
<html>
<head>
    <title>PEMESANAN TIKET</title>
</head>
<style type="text/css">
    ul{
        border-radius: 10px;
        list-style-type: none;
        margin: 0;
        padding: 1px;
        overflow: hidden;
        background-color: #324851;
    }
    li{
        float: left;
    }
    li a{
        display: block;
        color: white;
        text-align: center;
        padding: 10px 35px 10px;
        text-decoration: none;
    }
    li a:hover{
        background-color: #111111;
    }
</style>
<body>
<div>
    <ul>
        <li><a href="homeuser.php">HOME</a></li>
        <li><a href="pesan_tiket.php"> PEMESANAN TIKET </a></li>
        <li><a href="jadwal_keberangkatan.php">JADWAL KEBERANGKATAN</a></li>
        <li><a href="history.php">HISTORY PEMESANAN TIKET</a></li>
        <li style="float: right;"><a href="index.php">LOGOUT</a></li>
    </ul>
</div>
<div>
    <hr>
</div>
<div>
    <h2 style="padding-left: 110px;">DENAH KURSI BUS</h2>
    <img src="image/denah.jpg" style="float: left; padding-left: 50px;">
      <?php
include 'config.php';

//form pembelian
if(!isset($_POST['proses']) && !isset($_POST['bayar'])){
 echo '
 <center>
 <div><form method="post" action="proses_pesan.php" name="fform">
        <table border="1" style="margin-left: auto; margin-right: auto; width: 800px; height: 400px;">
        <tr><td colspan="2" style="background-color: #336B87; color: white; height: 25px; width: 45px; border-radius: 5px; text-align: center;"><h2>PEMESANAN TIKET</h2></td></tr>
        <tr>
            <td style="padding-left: 10px; padding-right: 10px;">ID PEMBELI : <input type="text" name="id_pembeli"></td>
            <td style="padding-left: 10px; padding-right: 10px;">TANGGAL : <select name="tgl" style="width: 185px; height: 25px;">
              <option value="" >pilih tanggal keberangkatan</option>
              <option>1 Agustus 2021</option>
              <option>2 Agustus 2021</option>
              <option>3 Agustus 2021</option>
              <option>4 Agustus 2021</option>
            </select></td>
        </tr>
        <tr>
            <td style="padding-left: 10px; padding-right: 10px;">NAMA : <input type="text" name="nama"></td>
            <td style="padding-left: 10px; padding-right: 10px;">JAM : <select name="jm" style="width: 185px; height: 25px;">
              <option value="" >pilih jam keberangkatan</option>
              <option>09:00 WIB</option>
              <option>15:00 WIB</option>
            </select></td>
        </tr>
        <tr>
            <td style="padding-left: 10px; padding-right: 10px;">KOTA KEBERANGKATAN : <select name="kota_berangkat" style="width: 185px; height: 25px;">
              <option value="" >pilih kota keberangkatan</option>
              <option>Jakarta</option>
              <option>Bandung</option>
              <option>Bogor</option>
              <option>Tegal</option>
              <option>Purwokerto</option>
              <option>Kebumen</option>
              <option>Purworejo</option>
              <option>Yogyakarta</option>
              <option>Solo</option>
              <option>Semarang</option>
              <option>Malang</option>
              <option>Surabaya</option>
            </select></td>
            <td style="padding-left: 10px; padding-right: 10px;">JUMLAH TIKET : <input type="text" name="jml"></td>
        <tr>
            <td style="padding-left: 10px; padding-right: 10px;">KOTA TUJUAN : <select name="kota_tujuan" style="width: 185px; height: 25px;">
              <option value="" >pilih kota tujuan</option>
              <option >Jakarta</option>
              <option >Bandung</option>
              <option>Bogor</option>
              <option>Tegal</option>
              <option>Purwokerto</option>
              <option>Kebumen</option>
              <option>Purworejo</option>
              <option>Yogyakarta</option>
              <option>Solo</option>
              <option >Semarang</option>
              <option>Malang</option>
              <option>Surabaya</option>
            </select></td>
            <td style="padding-left: 10px; padding-right: 10px;">NO. KURSI: <input type="text" name="no_kursi"></td></tr>
        </tr>
<tr><td colspan="2" style="text-align: center;"><input type="submit" name="proses" value="PESAN" style="background-color: #336B87; color: white; height: 25px; width: 60px; border-radius: 5px;"></td></tr>
    </table></form></div>
 ';
}
 
?>
</div>
<div style="padding-top: 40px;text-align: center; font-family: sans-serif;"><footer><b>Copyright &copy 2021 | Lisa Amalia</footer></div>
</body>
</html> 
